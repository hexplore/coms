package main

import (
	"context"
	"time"
	
	"bitbucket.org/hexplore/coms/client"
	"github.com/dkolbly/logging"
	"github.com/dkolbly/logging/pretty"
)

var log = logging.New("hexcli")

func main() {
	pretty.Debug()
	opt := &client.ServiceOptions{
		//HomeURL: "http://at.hexplore.us/",
		HomeURL: "http://localhost:8080/",
		ClientID: "$HEXCLI",
	}
	svc, err := client.Dial(opt)
	if err != nil {
		log.Fatal(err)
	}

	user := &client.SessionOptions{
		User:     "donovan",
		Password: "knock-knock",
	}
	session, err := svc.Connect(context.Background(), user)

	go func() {
		for s := range session.StatusChannel() {
			log.Info("at %s : %t %s",
				s.StatusTime.Format("2006-01-02 15:04:05"),
				s.Online,
				s.Note)
		}
	}()

	<- time.After(time.Second)
	session.Post("#living", "Turn down the music!")
	<- time.After(5 * time.Second)
}
