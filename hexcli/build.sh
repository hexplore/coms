#! /bin/bash

stamp=$(
    (
        echo -n "{"
        awk '$1 == "version" { printf("\"name\":\"%s\",", $2) }' ../version.info
        awk '$1 == "edition" { printf("\"edition\":\"%s\",", $2) }' ../version.info
        if test "$(git status --porcelain | wc -l)" != 0 ; then
            echo -n "\"modified\": true,"
        fi
        echo -n "\"commit\":\"$(git rev-parse HEAD)\","
        echo -n "\"branch\":\"$(git rev-parse --abbrev-ref HEAD)\","
        echo -n "\"time\":\"$(date -u +%Y-%m-%dT%H:%M:%SZ)\""
        echo -n "}"
    ) | base64 | tr -d '\012')

echo $stamp | base64 -d | jq .

go install -ldflags "-X bitbucket.org/hexplore/coms/client.Stamp=$stamp" bitbucket.org/hexplore/coms/hexcli
