See: https://github.com/gogo/protobuf/

go get github.com/gogo/protobuf/gogoproto
go get github.com/gogo/protobuf/proto
go get github.com/gogo/protobuf/jsonpb
go get github.com/gogo/protobuf/protoc-gen-gogo
go get github.com/gogo/protobuf/gogoproto
go install github.com/gogo/protobuf/protoc-gen-gogoslick

(cd $GOPATH/src ; protoc --gogoslick_out=. bitbucket.org/hexplore/coms/wire/at.proto)
