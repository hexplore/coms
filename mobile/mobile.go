// this creates a class called mobile.Mobile
package mobile

import (
	"context"
	"fmt"

	"bitbucket.org/hexplore/coms/client"
	"github.com/dkolbly/logging"
	"github.com/dkolbly/logging/pretty"
)

func init() {
	pretty.Debug()
}

var log = logging.New("mobile")

// this creates a static function in the Hello class called "hello"
func Hello(index int) string {
	switch index {
	case 1:
		return "First"
	default:
		return fmt.Sprintf("Hello #%d", index)
	}
}

func Funky(arg string) string {
	return fmt.Sprintf("..{%s}..", arg)
}

var events chan string
var session *client.Session

func Start(instanceID string, private string) {
	// this shows up in the android monitor as I/GoLog

	events = make(chan string, 5)

	opt := &client.ServiceOptions{
		//HomeURL:           "http://192.168.1.228:8080/",
		HomeURL: "http://at.hexplore.us/",
		ClientID:          instanceID,
		PrivateStorageDir: private,
	}
	svc, err := client.Dial(opt)
	if err != nil {
		log.Fatal(err)
	}
	v := client.GetVersion()
	if v != nil {
		log.Info("Starting %s (%.7s) %s instance=%s",
			v.Name, v.Commit, v.Time.Format("2006-01-02 15:04:05"),
			instanceID)
	} else {
		log.Info("Starting instance=%s", instanceID)
	}

	user := &client.SessionOptions{
		User:     "donovan",
		Password: "knock-knock",
	}
	s, err := svc.Connect(context.Background(), user)
	session = s
	go feeder(s)
}

func feeder(s *client.Session) {
	for s := range s.StatusChannel() {
		var msg string
		if s.Online {
			log.Info("online (%s)", s.Note)
			msg = "online"
		} else {
			log.Info("offline (%s)", s.Note)
			msg = "offline"
		}
		events <- fmt.Sprintf("%s %s %s",
			s.StatusTime.Format("01/02 15:04:05"),
			msg,
			s.Note)
	}
}

func Next() string {
	return <-events
}

func Send(msg string) {
	log.Info("sending %q", msg)
	session.Post("living", msg)
}
