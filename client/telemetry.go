package client

import (
	"bytes"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
)

func (svc *Service) uploadTelemetry(buf []byte) error {
	u := *svc.home
	u.Path = fmt.Sprintf("telemetry/%s", svc.clientID)
	prebuf := fmt.Sprintf(`{"fmt":1, "records":`)
	resp, err := http.Post(
		u.String(),
		"application/json",
		io.MultiReader(
			bytes.NewReader([]byte(prebuf)),
			bytes.NewReader(buf),
			bytes.NewReader([]byte(`}`))))
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return fmt.Errorf("upload failed (%d)", resp.StatusCode)
	}
	
	ack, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	n, err := strconv.Atoi(string(ack))
	if err != nil {
		return err
	}
	if n < 1 {
		return ErrUnexpectedResponse
	}
	return nil
}

var ErrUnexpectedResponse = errors.New("expected positive integer response")
