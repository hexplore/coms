package logdb

import (
	"encoding/json"
	"fmt"
	"runtime"
	"strings"
	"sync/atomic"
	"time"

	"github.com/dkolbly/logging"
)

type logup struct {
	Ser       int64           `json:"ser"`
	Seq       uint64          `json:"seq"`
	Timestamp time.Time       `json:"ts"`
	Module    string          `json:"module"`
	Level     uint8           `json:"level"`
	File      string          `json:"file,omitempty"`
	Line      int             `json:"line,omitempty"`
	Message   string          `json:"message"`
	Params    json.RawMessage `json:"params,omitempty"`
	Tags      []string        `json:"tags,omitempty"`
}

func (l *Store) Write(rec *logging.Record, skip int) {
	l.insert(rec, skip+1)
	l.next.Write(rec, skip+1)
}

func (s *Store) insert(rec *logging.Record, skip int) {
	_, file, line, _ := runtime.Caller(skip)

	msg := fmt.Sprintf(rec.Format, rec.Args...)

	var params []byte
	var tags []string
	if len(rec.Annotations) > 0 {
		pmap := make(map[string]interface{})
		for k, v := range rec.Annotations {
			if strings.HasPrefix(k, "tag.") {
				tags = append(tags, k[4:])
			} else {
				pmap[k] = v
			}
		}
		if len(pmap) > 0 {
			params, _ = json.Marshal(pmap)
		}
	}
	entry := logup{
		Seq:       rec.ID,
		Timestamp: rec.Timestamp,
		Module:    rec.Module,
		Level:     uint8(rec.Level),
		File:      file,
		Line:      line,
		Message:   msg,
		Params:    json.RawMessage(params),
		Tags:      tags,
	}
	select {
	case s.buffer <- entry:

	default:
		atomic.AddInt64(&s.lost, 1)
	}
}

func (s *Store) feed() {
	for msg := range s.buffer {
		var err error
		var tags []byte
		if len(msg.Tags) > 0 {
			tags, _ = json.Marshal(msg.Tags)
		}
		if msg.File != "" {
			_, err = s.db.Exec(
				`INSERT INTO 
                             logs(seq, nanos, module, level, file, line, msg, params, tags)
                         VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)`,
				msg.Seq,
				msg.Timestamp.UnixNano(),
				msg.Module,
				msg.Level,
				msg.File,
				msg.Line,
				msg.Message,
				[]byte(msg.Params),
				tags)
		} else {
			_, err = s.db.Exec(
				`INSERT INTO 
                             logs(seq, nanos, module, level, msg, params, tags)
                         VALUES(?, ?, ?, ?, ?, ?, ?)`,
				msg.Seq,
				msg.Timestamp.UnixNano(),
				msg.Module,
				msg.Level,
				msg.Message,
				[]byte(msg.Params),
				tags)
		}
		if err != nil {
			// can't really log it... we'll loop!
			fmt.Printf("ERROR: log insert failed: %s\n", err)
			return
		}
	}
}
