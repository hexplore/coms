// Code generated by go-bindata.
// sources:
// migration/0001_logs.up.sql
// migration/0002_tags.up.sql
// DO NOT EDIT!

package logdb

import (
	"bytes"
	"compress/gzip"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"time"
)

func bindataRead(data []byte, name string) ([]byte, error) {
	gz, err := gzip.NewReader(bytes.NewBuffer(data))
	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}

	var buf bytes.Buffer
	_, err = io.Copy(&buf, gz)
	clErr := gz.Close()

	if err != nil {
		return nil, fmt.Errorf("Read %q: %v", name, err)
	}
	if clErr != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

type asset struct {
	bytes []byte
	info  os.FileInfo
}

type bindataFileInfo struct {
	name    string
	size    int64
	mode    os.FileMode
	modTime time.Time
}

func (fi bindataFileInfo) Name() string {
	return fi.name
}
func (fi bindataFileInfo) Size() int64 {
	return fi.size
}
func (fi bindataFileInfo) Mode() os.FileMode {
	return fi.mode
}
func (fi bindataFileInfo) ModTime() time.Time {
	return fi.modTime
}
func (fi bindataFileInfo) IsDir() bool {
	return false
}
func (fi bindataFileInfo) Sys() interface{} {
	return nil
}

var __0001_logsUpSql = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\x6c\xcf\xb1\xca\xc2\x30\x10\xc0\xf1\xbd\x4f\x71\x63\x0b\xdf\xd0\x0f\x17\xc1\xe9\x5a\x8e\x1a\xb4\x55\x8e\x53\xec\x58\x30\x96\x42\xda\x6a\x83\x3e\xbf\x43\x4c\xe2\x60\xc6\x5f\xf2\x27\x77\x25\x13\x0a\x81\x60\xb1\x27\x30\x73\x6f\xd3\x04\x00\xc0\xea\x05\xfc\x51\x8d\x50\x45\x0c\x47\x56\x35\x72\x0b\x3b\x6a\x01\x4f\x72\x50\x4d\xc9\x54\x53\x23\x7f\x9f\xe4\x11\x92\x42\x55\xca\xfb\xd4\x4d\xb3\xfd\xe1\xa3\xed\xc3\x7b\xa1\x8b\xd7\xf9\xfa\x34\xda\xe9\x19\xb9\xdc\x22\xa7\xab\x3c\x73\x97\x46\xbf\xb4\x89\x53\xad\x9d\xde\x06\x1f\xc4\xe4\x3f\x0f\xcd\x30\xe9\xaf\x4d\x1c\xde\xbb\xa5\x1b\x6d\xfc\x3b\xc9\x36\xc9\x3b\x00\x00\xff\xff\x38\x13\x64\x9a\x0a\x01\x00\x00")

func _0001_logsUpSqlBytes() ([]byte, error) {
	return bindataRead(
		__0001_logsUpSql,
		"0001_logs.up.sql",
	)
}

func _0001_logsUpSql() (*asset, error) {
	bytes, err := _0001_logsUpSqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "0001_logs.up.sql", size: 266, mode: os.FileMode(436), modTime: time.Unix(1488047128, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

var __0002_tagsUpSql = []byte("\x1f\x8b\x08\x00\x00\x09\x6e\x88\x00\xff\x72\xf4\x09\x71\x0d\x52\x08\x71\x74\xf2\x71\x55\xc8\xc9\x4f\x2f\x56\x70\x74\x71\x51\x70\xf6\xf7\x09\xf5\xf5\x53\x28\x49\x4c\x2f\x56\x08\x71\x8d\x08\xb1\xe6\x02\x04\x00\x00\xff\xff\x1f\x9d\xb0\xba\x27\x00\x00\x00")

func _0002_tagsUpSqlBytes() ([]byte, error) {
	return bindataRead(
		__0002_tagsUpSql,
		"0002_tags.up.sql",
	)
}

func _0002_tagsUpSql() (*asset, error) {
	bytes, err := _0002_tagsUpSqlBytes()
	if err != nil {
		return nil, err
	}

	info := bindataFileInfo{name: "0002_tags.up.sql", size: 39, mode: os.FileMode(436), modTime: time.Unix(1488116615, 0)}
	a := &asset{bytes: bytes, info: info}
	return a, nil
}

// Asset loads and returns the asset for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func Asset(name string) ([]byte, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("Asset %s can't read by error: %v", name, err)
		}
		return a.bytes, nil
	}
	return nil, fmt.Errorf("Asset %s not found", name)
}

// MustAsset is like Asset but panics when Asset would return an error.
// It simplifies safe initialization of global variables.
func MustAsset(name string) []byte {
	a, err := Asset(name)
	if err != nil {
		panic("asset: Asset(" + name + "): " + err.Error())
	}

	return a
}

// AssetInfo loads and returns the asset info for the given name.
// It returns an error if the asset could not be found or
// could not be loaded.
func AssetInfo(name string) (os.FileInfo, error) {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	if f, ok := _bindata[cannonicalName]; ok {
		a, err := f()
		if err != nil {
			return nil, fmt.Errorf("AssetInfo %s can't read by error: %v", name, err)
		}
		return a.info, nil
	}
	return nil, fmt.Errorf("AssetInfo %s not found", name)
}

// AssetNames returns the names of the assets.
func AssetNames() []string {
	names := make([]string, 0, len(_bindata))
	for name := range _bindata {
		names = append(names, name)
	}
	return names
}

// _bindata is a table, holding each asset generator, mapped to its name.
var _bindata = map[string]func() (*asset, error){
	"0001_logs.up.sql": _0001_logsUpSql,
	"0002_tags.up.sql": _0002_tagsUpSql,
}

// AssetDir returns the file names below a certain
// directory embedded in the file by go-bindata.
// For example if you run go-bindata on data/... and data contains the
// following hierarchy:
//     data/
//       foo.txt
//       img/
//         a.png
//         b.png
// then AssetDir("data") would return []string{"foo.txt", "img"}
// AssetDir("data/img") would return []string{"a.png", "b.png"}
// AssetDir("foo.txt") and AssetDir("notexist") would return an error
// AssetDir("") will return []string{"data"}.
func AssetDir(name string) ([]string, error) {
	node := _bintree
	if len(name) != 0 {
		cannonicalName := strings.Replace(name, "\\", "/", -1)
		pathList := strings.Split(cannonicalName, "/")
		for _, p := range pathList {
			node = node.Children[p]
			if node == nil {
				return nil, fmt.Errorf("Asset %s not found", name)
			}
		}
	}
	if node.Func != nil {
		return nil, fmt.Errorf("Asset %s not found", name)
	}
	rv := make([]string, 0, len(node.Children))
	for childName := range node.Children {
		rv = append(rv, childName)
	}
	return rv, nil
}

type bintree struct {
	Func     func() (*asset, error)
	Children map[string]*bintree
}
var _bintree = &bintree{nil, map[string]*bintree{
	"0001_logs.up.sql": &bintree{_0001_logsUpSql, map[string]*bintree{}},
	"0002_tags.up.sql": &bintree{_0002_tagsUpSql, map[string]*bintree{}},
}}

// RestoreAsset restores an asset under the given directory
func RestoreAsset(dir, name string) error {
	data, err := Asset(name)
	if err != nil {
		return err
	}
	info, err := AssetInfo(name)
	if err != nil {
		return err
	}
	err = os.MkdirAll(_filePath(dir, filepath.Dir(name)), os.FileMode(0755))
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(_filePath(dir, name), data, info.Mode())
	if err != nil {
		return err
	}
	err = os.Chtimes(_filePath(dir, name), info.ModTime(), info.ModTime())
	if err != nil {
		return err
	}
	return nil
}

// RestoreAssets restores an asset under the given directory recursively
func RestoreAssets(dir, name string) error {
	children, err := AssetDir(name)
	// File
	if err != nil {
		return RestoreAsset(dir, name)
	}
	// Dir
	for _, child := range children {
		err = RestoreAssets(dir, filepath.Join(name, child))
		if err != nil {
			return err
		}
	}
	return nil
}

func _filePath(dir, name string) string {
	cannonicalName := strings.Replace(name, "\\", "/", -1)
	return filepath.Join(append([]string{dir}, strings.Split(cannonicalName, "/")...)...)
}

