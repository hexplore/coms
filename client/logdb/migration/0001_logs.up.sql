CREATE TABLE logs(
    ser         INTEGER PRIMARY KEY AUTOINCREMENT,
    seq         BIGINT,
    nanos       BIGINT,
    msg         TEXT,
    module      VARCHAR(30),
    level       INT8,
    file        VARCHAR(100),
    line        INT,
    params      TEXT
);
