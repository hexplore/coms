package logdb

//go:generate go-bindata -pkg logdb -prefix migration migration

import (
	"database/sql"
	"fmt"

	"github.com/dkolbly/logging"
	"github.com/mattes/migrate"
	"github.com/mattes/migrate/database"
	_ "github.com/mattes/migrate/database/sqlite"
	"github.com/mattes/migrate/source/go-bindata"
	_ "github.com/mattn/go-sqlite3"
)

var log = logging.New("logdb")

type Store struct {
	db   *sql.DB
	next logging.Writer
	buffer chan logup
	lost int64
}

func New(next logging.Writer) *Store {
	return &Store{
		next:   next,
		buffer: make(chan logup, 100),
	}
}

func (s *Store) Back(dbpath string) error {

	db, err := initdb(dbpath)
	if err != nil {
		log.Error("Failed to configure database logging on %s: %s",
			dbpath, err)
		return err
	}
	s.db = db
	go s.feed()
	sz, err := s.Size()
	if err == nil {
		log.Debug("Log database has %d pages, %d free",
			sz.PageCount, sz.FreeCount)
	}
	return nil
}

func (l *Store) Erase(mark int64) {
	_, err := l.db.Exec(`DELETE FROM logs WHERE ser <= ?`, mark)
	if err != nil {
		fmt.Printf("*** log trim failed: %s\n", err)
	}
}


func initdb(dbpath string) (*sql.DB, error) {

	migrationSources, err := bindata.WithInstance(&bindata.AssetSource{
		Names:     AssetNames(),
		AssetFunc: Asset,
	})
	if err != nil {
		log.Error("SR %s", err)
		return nil, err
	}

	databaseTarget, err := database.Open("sqlite3://" + dbpath)
	if err != nil {
		log.Error("TG %s", err)
		return nil, err
	}

	m, err := migrate.NewWithInstance(
		"<bindata>",
		migrationSources,
		dbpath,
		databaseTarget)
	if err != nil {
		log.Error("MIG %s", err)
		return nil, err
	}

	// Migrate all the way up; note that this returns an error
	// (ErrNoChange) if there was nothing to do
	err = m.Up()
	if err != nil && err != migrate.ErrNoChange {
		log.Error("UP %s", err)
		return nil, err
	}
	srcerr, dberr := m.Close()
	if srcerr != nil {
		log.Error("SE %s", srcerr)
		return nil, srcerr
	}
	if dberr != nil {
		log.Error("DE %s", dberr)
		return nil, dberr
	}

	x, e := sql.Open("sqlite3", dbpath)
	if e != nil {
		log.Error("OP %s", e)
	}
	return x, e
}
