package logdb

import (
	"bytes"
	"encoding/json"
	"fmt"
	"time"
)

type StatsReport struct {
	PageCount int64
	FreeCount int64
}

func (s *Store) Size() (*StatsReport, error) {
	var r StatsReport
	err := s.db.QueryRow(`PRAGMA page_count`).Scan(&r.PageCount)
	if err != nil {
		return nil, err
	}
	err = s.db.QueryRow(`PRAGMA freelist_count`).Scan(&r.FreeCount)
	if err != nil {
		return nil, err
	}
	return &r, nil
}


func (l *Store) Read(mark, limit int64) ([]byte, int64) {
	rows, err := l.db.Query(
		`SELECT ser, seq, nanos, module, level, msg, file, line, params, tags
		   FROM logs
		  WHERE ser > ?
                  ORDER BY ser
		  LIMIT ?`,
		mark, limit)
	if err != nil {
		// bummer
		fmt.Printf("ERROR: logs db query failed: %s\n", err)
		return nil, 0
	}
	var accum bytes.Buffer
	(&accum).WriteByte('[')
	var max int64
	first := true

	for rows.Next() {
		var up logup
		var nanos int64
		var params []byte
		var tags []byte
		err = rows.Scan(
			&up.Ser,
			&up.Seq,
			&nanos,
			&up.Module,
			&up.Level,
			&up.Message,
			&up.File,
			&up.Line,
			&params,
			&tags)
		if err != nil {
			fmt.Printf("ERROR: logdb scan failed: %s\n", err)
			return nil, 0
		}
		if params != nil {
			up.Params = json.RawMessage(params)
		}
		if tags != nil {
			json.Unmarshal(tags, &up.Tags)
		}

		up.Timestamp = time.Unix(
			nanos/1000000000,
			nanos%1000000000)
		buf, _ := json.Marshal(&up)
		if !first {
			(&accum).WriteByte(',')
		}
		(&accum).Write(buf)
		max = up.Ser
		first = false
	}
	if first {
		// no new logs
		return nil, 0
	}

	(&accum).WriteByte(']')
	return accum.Bytes(), max
}
