package client

import (
	"context"
	"errors"
	"net/url"
	"sync"
	"sync/atomic"
	"time"

	"bitbucket.org/hexplore/coms/wire"
	"github.com/dkolbly/logging"
	"github.com/gorilla/websocket"
)

func mustURL(rawurl string) *url.URL {
	u, err := url.Parse(rawurl)
	if err != nil {
		panic(err)
	}
	return u
}

var socketEndpoint = mustURL("/socket")

var log = logging.New("hexcc")

type SessionOptions struct {
	User     string
	Password string
}

type Session struct {
	owner      *Service
	home       *url.URL
	options    SessionOptions
	cnxnStatus chan CnxnStatus
	current    *cnxn
}

// Connect establishes a connection for the context of the given user/principal
func (svc *Service) Connect(ctx context.Context, opt *SessionOptions) (*Session, error) {
	sess := &Session{
		owner:      svc,
		home:       svc.home,
		cnxnStatus: make(chan CnxnStatus, 10),
	}
	if opt != nil {
		sess.options = *opt
	}

	sess.cnxnStatus <- CnxnStatus{
		StatusTime: time.Now(),
		Note:       "starting",
	}
	go sess.nailup(ctx)
	return sess, nil
}

type CnxnStatus struct {
	StatusTime  time.Time
	Note        string
	Failure     error
	FailTime    time.Time
	Online      bool
	OnlineSince time.Time
	Latency     time.Duration
}

func (sess *Session) StatusChannel() chan CnxnStatus {
	return sess.cnxnStatus
}

const initialBackoff = 1875 * time.Millisecond // 5 doublings => 1 minute

func (sess *Session) nailup(ctx context.Context) {
	backoff := initialBackoff
	attemptsStart := time.Now()
	attempts := 0
	for {
		attempts++
		cnxn, err := sess.attempt()
		if err != nil {
			now := time.Now()
			sess.cnxnStatus <- CnxnStatus{
				StatusTime: now,
				Note:       "attempt failed",
				Failure:    err,
				FailTime:   now,
			}
			<-time.After(backoff)
			backoff *= 2
			if backoff > time.Minute {
				backoff = time.Minute
			}
		} else {
			backoff = initialBackoff
			now := time.Now()
			log.Re(Tag("connectionup")).Info(
				"Connected after %d attempts over %s",
				attempts, now.Sub(attemptsStart))
			attempts = 0
			sess.current = cnxn
			sess.cnxnStatus <- CnxnStatus{
				StatusTime:  now,
				Note:        "connected",
				Online:      true,
				OnlineSince: now,
			}
			err := cnxn.operate()
			sess.current = nil
			t1 := time.Now()
			sess.cnxnStatus <- CnxnStatus{
				StatusTime:  t1,
				Note:        "disconnected",
				OnlineSince: now,
				Failure:     err,
			}
			log.Re(Tag("connectiondown")).Info("Disconnected after %s",
				t1.Sub(now))
			attemptsStart = t1
		}
	}
}

type cnxn struct {
	owner       *Session
	conn        *websocket.Conn
	tag         int32
	tx          chan wire.Frame
	pending     map[int32]*pender
	pendingLock sync.Mutex
}

func (sess *Session) attempt() (*cnxn, error) {
	d := websocket.Dialer{}
	u := sess.home.ResolveReference(socketEndpoint)
	u.Scheme = "ws"

	log.Re(Tag("connectionattempt")).Info("Calling %s", u)
	conn, _, err := d.Dial(u.String(), nil)
	if err != nil {
		log.Re(Tag("connectionfailed")).Error("Connection attempt failed: %s",
			err)
		return nil, err
	}
	return &cnxn{
		owner:   sess,
		conn:    conn,
		tx:      make(chan wire.Frame, 100),
		pending: make(map[int32]*pender, 10),
	}, nil
}

func (c *cnxn) operate() error {
	hello := &wire.Hello{
		Version:  "dev",
		ClientID: c.owner.owner.clientID,
	}
	v := GetVersion()
	if v != nil {
		hello.Version = v.Name
		hello.Commit = v.Commit
		hello.BuildTime = v.Time.Format(time.RFC3339)
	}
	c.sendHello(hello)

	go c.rxloop()
	c.txloop()
	return nil
}

func (c *cnxn) sendHello(msg *wire.Hello) {
	c.tx <- wire.Frame{
		Type:  wire.HELLO,
		Tag:   atomic.AddInt32(&c.tag, 1),
		Hello: msg,
	}
}

func (c *cnxn) txloop() {
	for msg := range c.tx {
		// send a message
		buf, err := msg.Marshal()
		if err != nil {
			log.Error("whoops")
			return
		}
		log.Info("Sending %s #%d - %d bytes", msg.Type, msg.Tag, len(buf))
		c.conn.WriteMessage(websocket.BinaryMessage, buf)
	}

}

func (c *cnxn) rxloop() {
	defer close(c.tx) // TODO this could result in a panic
	for {
		msgType, buf, err := c.conn.ReadMessage()
		if err != nil {
			log.Error("Read failure: %s", err)
			return
		}
		if msgType == websocket.BinaryMessage {
			err = c.dispatch(buf)
			if err != nil {
				log.Error("Processing error: %s", err)
				return
			}
		}
	}
}

func (c *cnxn) dispatch(buf []byte) error {
	var frame wire.Frame
	err := (&frame).Unmarshal(buf)
	if err != nil {
		log.Error("Decode error; most likely client is out of sync")
		return err
	}
	log.Info("Received %s #%d - %d bytes", frame.Type, frame.Tag, len(buf))
	switch frame.Type {
	case wire.HELLO, wire.POST:
		return ErrUnexpectedMessage
	case wire.ACK:
		return c.dispatchAck(frame.Tag, frame.Ack)
	default:
		log.Error("Unknown frame type %d", frame.Type)
		return ErrBadMessage
	}
}

func (c *cnxn) dispatchAck(tag int32, ack *wire.Ack) error {
	c.pendingLock.Lock()
	defer c.pendingLock.Unlock()
	if p, ok := c.pending[tag]; ok {
		p.reply <- ack
		delete(c.pending, tag)
		return nil
	}
	return ErrUnexpectedAck
}

var ErrUnexpectedAck = errors.New("unexpected ack")
var ErrUnexpectedMessage = errors.New("unexpected message")
var ErrNotConnected = errors.New("not connected")
var ErrBadMessage = errors.New("bad message")

func (sess *Session) rpc(f *wire.Frame) error {
	cnxn := sess.current
	if cnxn == nil {
		return ErrNotConnected
	}
	return cnxn.rpc(f)
}

type pender struct {
	reply chan *wire.Ack
}

func (c *cnxn) rpc(f *wire.Frame) error {
	tag := atomic.AddInt32(&c.tag, 1)
	pend := &pender{
		reply: make(chan *wire.Ack),
	}
	c.pendingLock.Lock()
	c.pending[tag] = pend
	c.pendingLock.Unlock()
	tmp := *f
	tmp.Tag = tag
	log.Info("Issuing #%d %s", tag, tmp.Type)
	c.tx <- tmp

	ack := <-pend.reply
	log.Info("Got reply")
	if ack.Error == "" {
		return nil
	}
	return AckError{ack}
}

type AckError struct {
	ack *wire.Ack
}

func (a AckError) Error() string {
	return a.ack.Error
}

func (sess *Session) Post(room, text string) error {
	return sess.rpc(&wire.Frame{
		Type: wire.POST,
		Post: &wire.Post{
			Room: room,
			Text: text,
		},
	})
}
