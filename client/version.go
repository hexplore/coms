package client

import (
	"encoding/base64"
	"encoding/json"
	"time"

	"github.com/dkolbly/logging"
)

var Stamp string

type Version struct {
	Name    string    `json:"name"`
	Edition string    `json:"edition,omitempty"`
	Commit  string    `json:"commit"`
	Branch  string    `json:"branch"`
	Time    time.Time `json:"time"`
}

func GetVersion() *Version {
	var v Version
	buf, err := base64.StdEncoding.DecodeString(Stamp)
	if err != nil {
		return nil
	}

	err = json.Unmarshal(buf, &v)
	if err != nil {
		return nil
	}
	return &v
}

func (v *Version) Annotate(rec *logging.Record) {
	if v == nil {
		return
	}
	rec.Annotate("x-version-name", v.Name)
	if v.Edition != "" {
		rec.Annotate("x-version-edition", v.Edition)
	}
	rec.Annotate("x-commit", v.Commit)
	rec.Annotate("x-branch", v.Branch)
	rec.Annotate("x-build-time", v.Time)
}
