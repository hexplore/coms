package client

import (
	"net/url"
	"time"
)

type Service struct {
	home       *url.URL
	clientID   string
	privateDir string
}

type ServiceOptions struct {
	HomeURL           string
	ClientID          string
	PrivateStorageDir string
}

// Dial establishes a relationship to a service
func Dial(opt *ServiceOptions) (*Service, error) {
	var home = "http://at.hexplore.us/"
	var priv = "/tmp"
	var cid string

	if opt != nil {
		if opt.HomeURL != "" {
			home = opt.HomeURL
		}
		if opt.ClientID != "" {
			cid = opt.ClientID
		}
		if opt.PrivateStorageDir != "" {
			priv = opt.PrivateStorageDir
		}
	}

	u, err := url.Parse(home)
	if err != nil {
		return nil, err
	}

	svc := &Service{
		home:       u,
		privateDir: priv,
		clientID:   cid,
	}
	setupLogCapture(priv)

	log.Re(GetVersion()).Info("Client dialing %s", home)

	go func() {
		var mark int64
		firstError := true
		for range time.NewTicker(time.Second).C {
			buf, last := uploadChunk(mark)
			if buf != nil {
				err := svc.uploadTelemetry(buf)
				if err == nil {
					// commit the upload
					mark = last
					forgetUpTo(last)
					firstError = true
				} else if firstError {
					log.Error("Failed to upload: %s", err)
					firstError = false
				}
			}
		}
	}()
	return svc, nil
}
