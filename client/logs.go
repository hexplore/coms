package client

import (
	"fmt"
	"path"
	"sync"

	"github.com/dkolbly/logging"
	_ "github.com/dkolbly/logging/pretty" // include so our init() happens later
	"bitbucket.org/hexplore/coms/client/logdb"
)

var logstore *logdb.Store
var setupOnce sync.Once

func init() {
	logstore = logdb.New(logging.DefaultBackend.Target)
	logging.DefaultBackend.Target = logstore
}

func setupLogCapture(privateDir string) {
	dbpath := path.Join(privateDir, "logs.sq3")
	setupOnce.Do(func() {
		if logstore != nil {
			logstore.Back(dbpath)
		}
	})
}

func uploadChunk(from int64) ([]byte, int64) {
	if logstore == nil {
		return nil, 0
	}
	
	return logstore.Read(from, 10)
}

func forgetUpTo(upto int64) {
	if logstore == nil {
		return
	}
	logstore.Erase(upto)
}



type annotateTrue struct {
	key string
}

// Annotate implements logging.Annotater, so that
// we can write log.Re(annotateTrue{...}).Debug(...)
func (a annotateTrue) Annotate(rec *logging.Record) {
	fmt.Printf("Tagging [%s]\n", a.key)
	rec.Annotations[a.key] = true
}

// Build a tag annotater, so we can write log.Re(Tag("foo")).Info(...)
// to tag a log message with "foo"
func Tag(k string) logging.Annotater {
	return annotateTrue{"tag." + k}
}
